
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Case</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="#">Page 1</a></li>
      <li><a href="#">Page 2</a></li>
      <li><a href="#">Page 3</a></li>
    </ul>
  </div>
</nav>
  
<div class="container">
  	
  	<div class="col-sm-6">
		<form method="post" action="">
			<div class="form-group row">
			  	<label for="example-text-input" class="col-xs-4 col-form-label">Full Name</label>
				<div class="col-xs-10">
					<input class="form-control" type="text" value="" name="name">
				</div>
			</div>
			<div class="form-group row">
			  <label for="example-search-input" class="col-xs-4 col-form-label">User</label>
				<div class="col-xs-10">
					<input class="form-control" type="text" value="" name="user">
				</div>
			</div>
			<div class="form-group row">
			  <label for="example-email-input" class="col-xs-4 col-form-label">Password</label>
				<div class="col-xs-10">
			    	<input class="form-control" type="text" value="" name="password">
				</div>
			</div>
			<div class="form-group row">
			  <label for="example-url-input" class="col-xs-4 col-form-label">Phone</label>
				<div class="col-xs-10">
			    	<input class="form-control" type="text" value="" name="phone">
				</div>
			</div>
			<div class="form-group row">
			  <label for="example-url-input" class="col-xs-4 col-form-label">Type</label>
				<div class="col-xs-10">
					<div class="form-group">
					  <select class="form-control" name="type">
					    <option value="Field Worker">Field Worker</option>
					    <option value="Supervisor">Supervisor</option>
					  </select>
					</div>
				</div>
			</div>
			<div class="form-group row" style="margin-left: 150px;">
				<input type="submit" class="btn btn-info" value="submit" name="submit">
			</div>
		</form>
	</div>

	<div class="col-sm-6">
		<table class="table table-bordered">
		    <thead>
		      <tr>
		        <th>Name</th>
		        <th>Phone</th>
		        <th>Type</th>
		      </tr>
		    </thead>
		    <tbody>
		    <?php
		    	include('user.php');
		    	$db = getDB();
				$sql = "SELECT * FROM users";

				$result = $db->prepare($sql); 
				$result->execute(); 

				foreach ($result as $row) {
					echo '<tr>';
					echo '<td>'.$row["name"];
					echo '<td>'.$row["phone"];
					echo '<td>'.$row["type"];
					echo '<tr>';
				}
				echo '</tbody>';
		    ?>	
		</table>	
	</div>

</body>
</html>



<?php
	//include('config.php');
	if(isset($_POST['submit'])){
		$newUser = new user();
		$newUser->insertUserData($_POST['name'], $_POST['user'], $_POST['password'], $_POST['phone'], $_POST['type']);
		header("Refresh:0");

	}
?>

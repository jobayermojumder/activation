<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Case</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="#">Page 1</a></li>
      <li><a href="#">Page 2</a></li>
      <li><a href="#">Page 3</a></li>
    </ul>
  </div>
</nav>
  
<div class="container">
  	
	  <div class="col-sm-10">
			<form method="post" action="">
				<div class="form-group row">
				  	<label for="example-text-input" class="col-xs-2 col-form-label">Name of Shop</label>
					<div class="col-xs-4">
						<input class="form-control" type="text" value="" name="shopName">
					</div>
				</div>
				<div class="form-group row">
				  <label for="example-search-input" class="col-xs-2 col-form-label">Shop Owner Name</label>
					<div class="col-xs-4">
						<input class="form-control" type="text" value="" name="shopOwner">
					</div>
				</div>
				<div class="form-group row">
				  <label for="example-email-input" class="col-xs-2 col-form-label">Address</label>
					<div class="col-xs-2">
						<div class="form-group">
						  <select class="form-control" name="district">
						    <option >Dhaka</option>
						    <option >Comilla</option>
						  </select>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="form-group">
						  <select class="form-control" name="thana">
						    <option >Thana 1</option>
						    <option >Thana 2</option>
						  </select>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="form-group">
						 	<input class="form-control" type="text" value="" name="details">
						 </div>
					</div>
				</div>


				<div class="form-group row">
				  <label for="example-url-input" class="col-xs-2 col-form-label">Ispahani Tea</label>
					<div class="col-xs-4">
				    	<input type="radio" name="isIspahani" value="yes"> Yes<br>
						<input type="radio" name="isIspahani" value="no"> No<br>
					</div>
				</div>


				<div class="form-group row">
				  <label for="example-url-input" class="col-xs-2 col-form-label">Phone</label>
					<div class="col-xs-4">
				    	<input class="form-control" type="text" value="" name="phone">
					</div>
				</div>
				<div class="form-group row">
				  <label for="example-url-input" class="col-xs-2 col-form-label">Reference</label>
					<div class="col-xs-4">
				    	<input class="form-control" type="text" value="" name="reference">
					</div>
				</div>
				<div class="form-group row" style="margin-left: 150px;">
					<input type="submit" class="btn btn-info" value="submit" name="submit">
				</div>
			</form>
		</div>
	</div>

</body>
</html>

<?php
	include('fieldadd.php');
	if(isset($_POST['submit'])){
		$newUser = new field();
		$newUser->insertData($_POST['shopName'], $_POST['shopOwner'], $_POST['district'], $_POST['thana'], $_POST['details'], $_POST['isIspahani'], $_POST['phone'], $_SESSION["reference"]);
		

	}
?>